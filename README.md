# Wardrobify

Team:

* Lavanya - Hats microservice
* Kristian (Kallen) 2 - Shoes microservice

## Design
We will be using bootstrap for styling.

## Shoes microservice

We will have a Shoes model with a BinVo pointing to the wardrobe microservice. This will poll bin information from the bin model in wardrobe microservice to be used in our shoes microcoservice.


## Hats microservice

We will have a model for Hats which will have a value object of location to poll information from the locations model from the wardrobe microservice. We will also use Pexels to obtain photos of hats to be displayed in hats list. We have made a form to create hats as well and add it to the Hats List on submit. In hats list, will show details of the hats we have and their locations, as well as give options to delete our hats.
