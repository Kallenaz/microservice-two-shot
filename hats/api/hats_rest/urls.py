from .views import list_hats, show_hat
from django.urls import path

urlpatterns = [
    path("locations/<int:location_vo_id>/hats/",list_hats, name="list_hats"),
    path("hats/<int:pk>/", show_hat, name="show_hat"),
    path("hats/", list_hats, name="create_hat"),
]
