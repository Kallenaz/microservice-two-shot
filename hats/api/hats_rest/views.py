import json
from .models import LocationVO, Hat
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .acls import get_photo
# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name","import_href", "id",]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "id", "picture_url","fabric", "color","location"]
    encoders={"location": LocationVODetailEncoder(),}
    # def get_extra_data(self, o):
        # return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "fabric", "color", "picture_url","id","location"]
    encoders={"location": LocationVODetailEncoder(),}


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
            return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        photo=get_photo(content["style_name"],content["fabric"],content["color"])
        content.update(photo)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_hat(request, pk):
    try:
        hat = Hat.objects.get(pk=pk)
    except Hat.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid hat id"},
            status=404,
        )

    if request.method == "GET":
        hat = Hat.objects.get(pk=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Hat.objects.filter(pk=pk).delete()
        updatedHats=Hat.objects.all()
        return JsonResponse(
            {"deleted": count > 0, "hats": updatedHats},
            encoder=HatListEncoder,
            safe=False,
        )
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "location" in content:
    #             location = LocationVO.objects.get(pk=content["location"])
    #             content["location"] = location
    #     except LocationVO.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid location id"},
    #             status=404,
    #         )
    #     Hat.objects.filter(pk=id).update(**content)
    #     return JsonResponse(
    #         {"hat": hat},
    #         encoder=HatDetailEncoder,
    #         safe=False,)
