# Generated by Django 4.0.3 on 2023-06-01 17:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hat',
            old_name='name',
            new_name='style_name',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='description',
        ),
        migrations.AddField(
            model_name='hat',
            name='color',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='hat',
            name='fabric',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
