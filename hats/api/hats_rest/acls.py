import os
import json
import requests

PEXELS_API_KEY = os.environ["PEXELS_API_KEY"]

def get_photo(style_name,fabric,color):
    headers={"Authorization": PEXELS_API_KEY}
    params={"query": style_name + " "+ fabric + " " + color}
    url="https://api.pexels.com/v1/search"
    response=requests.get(url,params=params, headers=headers)
    content= response.content
    parsed_json=json.loads(content)
    picture=parsed_json["photos"][0]["src"]["original"]
    try:
        return {"picture_url": picture}
    except (KeyError,IndexError):
        return {"picture_url": None}
