import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function ShoesList(props) {
    const [shoes, setShoes] = useState([]);

    async function fetchShoes() {
      const response = await fetch('http://localhost:8080/api/shoes/');

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setShoes(data.shoes);
      }
    }

    useEffect(() => {
      fetchShoes();
    }, [])

    async function deleteShoe(e,id) {
      //keeps page from refreshing
      e.preventDefault();

      const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {method: 'DELETE'});

      if (response.ok) {
        const data = await response.json();
        //rerender shoes list
        setShoes(data.shoes);
      }
    }

    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin Closet</th>
            <th>Delete Shoe</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.href}>
                <td>
                  <img src={shoe.picture_url} width="200px" height="200px" className="img-thumbnail" />
                </td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }
                </td>
                <td>
                  <button type="button" onClick={(e) => deleteShoe(e, shoe.id)} className="btn btn-danger">Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
