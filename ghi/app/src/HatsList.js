import React, { useState, useEffect } from "react";

function HatsList(props) {
    const [hats, setHats] = useState([]);
    async function fetchHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setHats(data.hats);
        }
    }
    useEffect(() => {
        fetchHats();
    }, [])

    async function deleteHat(e, id) {
        e.preventDefault();
        const response = await fetch(`http://localhost:8090/api/hats/${id}`,
            { method: 'DELETE' });

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }



    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>
                                <img src={hat.picture_url} alt="" width="200" height="200" />
                            </td>
                            <td>
                                {hat.style_name}
                            </td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>Closet Name: {hat.location.closet_name}</td>
                            <td>
                                <button type="button" onClick={(e) => deleteHat(e, hat.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody >
        </table >
    );
}
export default HatsList;
