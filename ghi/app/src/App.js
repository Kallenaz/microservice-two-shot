import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatList from './HatsList';
// import HatDetails from './HatDetails';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />}/>
            <Route path="new" element={<ShoeForm />}/>
          </Route>
          <Route path="hats">
            <Route index element={<HatList hats={props.hats} />}/>
            {/* <Route path=":id" element={<HatDetails hat={props.hat}/>}/> */}
            <Route path="new" element={<HatForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
