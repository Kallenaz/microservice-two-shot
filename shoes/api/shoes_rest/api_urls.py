from django.urls import path
from .views import api_list_shoes, api_show_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_all_shoes"),
    path(
        "bins/<int:bin_vo_id>/bins/",
        api_list_shoes,
        name="api_list_shoes_in_bin",
    ),
    path("shoes/<int:pk>/", api_show_shoe, name="api_show_shoe"),
]
