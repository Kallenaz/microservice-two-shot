#from .key import PEXELS_API_KEY
import requests
import json
import os

def get_photo(manufacturer, model_name, color):
    headers = {"Authorization": os.environ['PEXELS_API_KEY']}
    params = {
        "per_page": 1,
        "query": f"shoe {manufacturer} {model_name} {color}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
