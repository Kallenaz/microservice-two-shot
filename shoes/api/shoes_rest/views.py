from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href", "id"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer", "model_name", "color", "picture_url", "id", "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

    #would need to change models to use
    # def get_extra_data(self, o):
    #     return {
    #         "bin":
    #             {
    #                 "href": o.bin.import_href,
    #                 "closet_name": o.bin.closet_name,
    #                 "bin_number": o.bin.bin_number,
    #                 "bin_size": o.bin.bin_size,

    #             }
    #         }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            #study
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        print(content)
        photo = get_photo(content["manufacturer"], content["model_name"], content["color"])
        content.update(photo)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        request.method == "DELETE"
        count, _ = Shoe.objects.filter(pk=pk).delete()
        updatedShoes = Shoe.objects.all()
        return JsonResponse(
            {"deleted": count > 0, "shoes": updatedShoes},
            encoder=ShoeListEncoder,
            safe=False,
        )

    # else:
    #     content = json.loads(request.body)
    #     try:
    #         if "bin" in content:
    #             bin = BinVO.objects.get(pk=content["bin"])
    #             content["bin"] = bin
    #     except BinVO.DoesNotExist:
    #         return JsonResponse(
    #             {"message": "Invalid bin"},
    #             status=400,
    #         )

    #     Shoe.objects.filter(pk=pk).update(**content)
    #     shoe = Shoe.objects.get(pk=pk)
    #     return JsonResponse(
    #         shoe,
    #         encoder=ShoeDetailEncoder,
    #         safe=False,
    #     )
